# Инструкции:

- Поставить [node.js](https://nodejs.org/en/)
---
- Скачать архив:
    ```sh
    $ cd Documents/my_project
    $ git clone https://Kost9in@bitbucket.org/Kost9in/gulp.git
    ```
- или [скачать тут](https://bitbucket.org/Kost9in/gulp/get/master.zip)
---
- Перейти в папку проекта и выполнить установку зависимостей:
    ```sh
    $ cd Documents/my_project/gulp
    $ npm i
    ```
---
- Установить gulp глобально(только один раз):
    ```sh
    $ npm i gulp -g
    ```
---
- Запустить gulp:
    - без минификации:
        ```sh
        $ npm run dev
        ```
    - с минификацией:
        ```sh
        $ npm run prod
        ```